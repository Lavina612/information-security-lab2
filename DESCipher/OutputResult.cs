﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DESCipher
{
    public class OutputResult
    {
        public static void PrintBitsRow(List<byte> bits)
        {
            foreach (var bit in bits)
            {
                Console.Write(bit + " ");
            }
            Console.WriteLine();
        }

        public static void PrintBitsTable(List<byte> bits, int rowLength)
        {
            for (int i = 0; i < bits.Count; i = i + rowLength)
            {
                PrintBitsRow(bits.GetRange(i, rowLength));
            }
            Console.WriteLine();
        }

        public static void PrintKeys(List<List<byte>> keys)
        {
            foreach (var key in keys)
            {
                PrintBitsRow(key);
            }
            Console.WriteLine();
        }

        public static void PrintToFile(List<byte> bytes, int rowLength, string message, string filePath)
        {
            using(StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(message);

                for (int i = 0; i < bytes.Count; i++)
                {
                    writer.Write(bytes[i] + " ");
                    if ((i + 1) % rowLength == 0)
                    {
                        writer.WriteLine();
                    }
                }

                writer.WriteLine();
            }
        }

        public static void PrintToFile(List<byte> bytes, string message, string filePath)
        {
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(message);

                for (int i = 0; i < bytes.Count; i++)
                {
                    writer.Write($"{i.ToString().PadLeft(3, ' ')}");
                }

                writer.WriteLine();

                for (int i = 0; i < bytes.Count; i++)
                {
                    writer.Write($"{bytes[i].ToString().PadLeft(3, ' ')}");
                }

                writer.WriteLine();
                writer.WriteLine();
            }
        }
    }
}
