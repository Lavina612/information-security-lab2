﻿using System.Collections.Generic;
using System.Linq;

namespace DESCipher
{
    public class Keys
    {
        private const int KeysCount = 16;
        private const int KeyHalfSize = 28;

        public List<List<byte>> EncryptKeys { get; }

        public List<List<byte>> DecryptKeys { get; }

        public Keys(string key)
        {
            var keyBits = key.SelectMany(x => NumberSystemOperation.GetBinaryFromDecimalRepresentation((byte)x)).ToList();
            EncryptKeys = GenerateKeys(keyBits);
            DecryptKeys = new List<List<byte>>();
            DecryptKeys.AddRange(EncryptKeys);
            DecryptKeys.Reverse();
        }

        private List<List<byte>> GenerateKeys(List<byte> keyBits)
        {
            var extendedKey = ExtendKey(keyBits);
            var keyAfterInitialPermutation = Tables.Permutation(extendedKey, Tables.InitialKeyPermutation);

            var changableKey = new List<byte>();
            changableKey.AddRange(keyAfterInitialPermutation);

            var keysAfterShifts = KeyLeftCyclicShifts(changableKey);

            var finalKeys = new List<List<byte>>();
            foreach (var keyAfterShifts in keysAfterShifts)
            {
                finalKeys.Add(Tables.Permutation(keyAfterShifts, Tables.FinalKeyPermutation));
            }

            return finalKeys;
        }

        private List<byte> ExtendKey(List<byte> keyBits)
        {
            var extendedKey = new List<byte>();
            var unitCount = 0;
            for (int i = 0; i < keyBits.Count; i++)
            {
                unitCount += keyBits[i];
                extendedKey.Add(keyBits[i]);

                if ((i + 1) % 7 == 0)
                {
                    extendedKey.Add((byte)((unitCount + 1) % 2));
                    unitCount = 0;
                }
            }

            return extendedKey;
        }

        private List<List<byte>> KeyLeftCyclicShifts(List<byte> changableKey)
        {
            var keysAfterShifts = new List<List<byte>>();

            for (int i = 0; i < KeysCount; i++)
            {
                var c = changableKey.GetRange(0, KeyHalfSize);
                var cAfterShift = c.GetRange(Tables.KeyShift[i], KeyHalfSize - Tables.KeyShift[i]);
                cAfterShift.AddRange(c.GetRange(0, Tables.KeyShift[i]));

                var d = changableKey.GetRange(28, 28);
                var dAfterShift = d.GetRange(Tables.KeyShift[i], KeyHalfSize - Tables.KeyShift[i]);
                dAfterShift.AddRange(d.GetRange(0, Tables.KeyShift[i]));

                changableKey = cAfterShift;
                changableKey.AddRange(dAfterShift);

                keysAfterShifts.Add(changableKey);
            }

            return keysAfterShifts;
        }
    }
}