﻿using System.Collections.Generic;

namespace DESCipher
{
    public class DESCipher
    {
        public static int BlockSize { get; } = 64; 
        private const int MainTransformationLoopCount = 16;

        public static List<byte> Encrypt(List<byte> bytes, List<List<byte>> keys)
        {
            var bitsAfterInitialPermutation = Tables.Permutation(bytes, Tables.MainInitialPermutation);
            var bitsAfterMainTransformation = EncryptMainTransformation(bitsAfterInitialPermutation, keys);
            var bitsAfterFinalPermutation = Tables.Permutation(bitsAfterMainTransformation, Tables.MainFinalPermutation);
            
            return bitsAfterFinalPermutation;
        }

        private static List<byte> EncryptMainTransformation(List<byte> bytes, List<List<byte>> keys)
        {
            var changableBytes = new List<byte>();
            changableBytes.AddRange(bytes);

            for (int i = 0; i < MainTransformationLoopCount; i++)
            {
                var l = changableBytes.GetRange(0, 32);
                var r = changableBytes.GetRange(32, 32);

                var feistelFunctionResult = FeistelConversion.FeistelFunction(r, keys[i]);
                var newR = NumberSystemOperation.XOR(l, feistelFunctionResult);

                changableBytes = new List<byte>();
                changableBytes.AddRange(r);
                changableBytes.AddRange(newR);
            }

            return changableBytes;
        }

        public static List<byte> Decrypt(List<byte> bytes, List<List<byte>> keys)
        {
            var bitsAfterInitialPermutation = Tables.Permutation(bytes, Tables.MainInitialPermutation);
            var bitsAfterMainTransformation = DecryptMainTransformation(bitsAfterInitialPermutation, keys);
            var bitsAfterFinalPermutation = Tables.Permutation(bitsAfterMainTransformation, Tables.MainFinalPermutation);

            return bitsAfterFinalPermutation;
        }

        private static List<byte> DecryptMainTransformation(List<byte> bytes, List<List<byte>> keys)
        {
            var changableBytes = new List<byte>();
            changableBytes.AddRange(bytes);

            for (int i = 0; i < MainTransformationLoopCount; i++)
            {
                var l = changableBytes.GetRange(0, 32);
                var r = changableBytes.GetRange(32, 32);

                var feistelFunctionResult = FeistelConversion.FeistelFunction(l, keys[i]);
                var newL = NumberSystemOperation.XOR(r, feistelFunctionResult);

                changableBytes = new List<byte>();
                changableBytes.AddRange(newL);
                changableBytes.AddRange(l);
            }

            return changableBytes;
        }
    }
}
