﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DESCipher
{
    public class Program
    {
        private const int byteSize = 8;

        public static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Name of file with text to encrypt should be the first argument for program");
                return;
            }

            var filePath = args[0];
            string textToEncrypt;
            try
            {
                textToEncrypt = ReadFile(filePath);
            }
            catch(Exception)
            {
                return;
            }

            Console.WriteLine("Enter the key (only 56 bits = 7 characters)");
            var key = Console.ReadLine();
            while (key.Length != 7)
            {
                Console.WriteLine("Key should contain only 56 bits (7 characters). Please, try again.");
                key = Console.ReadLine();
            }
            Console.WriteLine();

            var bitsToEncrypt = textToEncrypt.SelectMany(x => NumberSystemOperation.GetBinaryFromDecimalRepresentation((byte)x)).ToList();
            if (bitsToEncrypt.Count % DESCipher.BlockSize != 0)
            {
                var additionalBitsCount = DESCipher.BlockSize - (bitsToEncrypt.Count % DESCipher.BlockSize);
                for(int i = 0; i < additionalBitsCount; i++)
                {
                    bitsToEncrypt.Add(0);
                }
            }

            var keys = new Keys(key);
            var encryptedBits = new List<byte>();
            for (int i = 0; i < bitsToEncrypt.Count / DESCipher.BlockSize; i++)
            {
                encryptedBits.AddRange(DESCipher.Encrypt(bitsToEncrypt.GetRange(i * DESCipher.BlockSize, DESCipher.BlockSize), keys.EncryptKeys));
            }

            var encryptedText = new StringBuilder();
            for (int i = 0; i < encryptedBits.Count; i = i + byteSize)
            {
                encryptedText.Append((char)NumberSystemOperation.GetDecimalFromBinaryRepresentation(encryptedBits.GetRange(i, byteSize)));
            }

            var decryptedBits = new List<byte>();
            for (int i = 0; i < encryptedBits.Count / DESCipher.BlockSize; i++)
            {
                decryptedBits.AddRange(DESCipher.Decrypt(encryptedBits.GetRange(i * DESCipher.BlockSize, DESCipher.BlockSize), keys.DecryptKeys));
            }

            var decryptedText = new StringBuilder();
            for (int i = 0; i < decryptedBits.Count; i = i + byteSize)
            {
                decryptedText.Append((char)NumberSystemOperation.GetDecimalFromBinaryRepresentation(decryptedBits.GetRange(i, byteSize)));
            }

            Console.WriteLine($"Text to encrypt: {textToEncrypt}");
            Console.WriteLine($"Encrypted text: {encryptedText}");
            Console.WriteLine($"Decrypted text: {decryptedText}");
        }

        static string ReadFile(string filePath)
        {
            var textToEncode = string.Empty;

            try
            {
                using (var streamReader = new StreamReader(filePath))
                {
                    textToEncode = streamReader.ReadToEnd();
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File not found");
                throw e;
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("File path should not be empty");
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine("IOException was occurred");
                throw e;
            }

            return textToEncode;
        }
    }
}
