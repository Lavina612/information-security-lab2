﻿using System;
using System.Collections.Generic;

namespace DESCipher
{
    public class NumberSystemOperation
    {
        private const int MaxByteValue = 255;

        public static List<byte> GetBinaryFromDecimalRepresentation(byte number)
        {
            var bits = new List<byte>();

            for (int i = 7; i >= 0; i--)
            {
                bits.Add((byte)((number >> i) & 1));
            }

            return bits;
        }

        public static int GetDecimalFromBinaryRepresentation(List<byte> bits)
        {
            var number = 0;
            for (int i = 0; i < bits.Count; i++)
            {
                number = number + (int)Math.Pow(2, i) * bits[bits.Count - i - 1];
            }

            return number;
        }

        public static List<byte> XOR(List<byte> firstBits, List<byte> secondBits)
        {
            var XORresult = new List<byte>();

            for (int i = 0; i < firstBits.Count; i++)
            {
                XORresult.Add((byte)(firstBits[i] ^ secondBits[i]));
            }

            return XORresult;
        }
    }
}
