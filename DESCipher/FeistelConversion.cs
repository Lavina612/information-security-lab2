﻿using System.Collections.Generic;

namespace DESCipher
{
    public class FeistelConversion
    {
        public static List<byte> FeistelFunction(List<byte> bits, List<byte> key)
        {
            var bitsAfterInitialPermutation = Tables.Permutation(bits, Tables.FeistelExtension);
            var bitsAfterXORWithKey = NumberSystemOperation.XOR(bitsAfterInitialPermutation, key);
            var bitsAfterFeistelTransformation = FeistelTransformation(bitsAfterXORWithKey);
            var bitsAfterFinalPermutation = Tables.Permutation(bitsAfterFeistelTransformation, Tables.FinalFeistelPermutation);

            return bitsAfterFinalPermutation;
        }

        private static List<byte> FeistelTransformation(List<byte> bytes)
        {
            var bytesAfterTransformation = new List<byte>();

            for (int i = 0; i < bytes.Count; i = i + 6)
            {
                var block = bytes.GetRange(i, 6);
                var row = NumberSystemOperation.GetDecimalFromBinaryRepresentation(new List<byte> { block[0], block[5] });
                var column = NumberSystemOperation.GetDecimalFromBinaryRepresentation(block.GetRange(1, 4));
                var ceilByIndeces = Tables.FeistelConversion[i / 6, row, column];
                
                bytesAfterTransformation.AddRange(NumberSystemOperation.GetBinaryFromDecimalRepresentation(ceilByIndeces));
            }

            return bytesAfterTransformation;
        }
    }
}
