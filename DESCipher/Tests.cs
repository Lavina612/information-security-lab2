﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DESCipher
{
    class Tests
    {
        public static void TestAllMethods()
        {
            byte[] bytesForPermutation =
            {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
                17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
                33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
                49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64
            };

            byte[] key =
            {
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 1,
                0, 0, 0, 0, 0, 1, 1,
                0, 0, 0, 0, 1, 1, 1,
                0, 0, 0, 1, 0, 1, 0,
                0, 0, 1, 0, 1, 0, 1,
                0, 1, 1, 1, 0, 1, 0,
                1, 0, 1, 1, 1, 1, 0
            };

            Console.WriteLine("Binary representation test");
            var bytes = new List<byte> { 3, 10, 15, 16, 17, 127, 192, 255 };
            foreach(var b in bytes)
            {
                Console.Write(NumberSystemOperation.GetBinaryFromDecimalRepresentation(b) + " ");
            }
            Console.WriteLine();

            Console.WriteLine("Decimal representation test");
            var bytesForDecimal = new List<byte> { 1, 0, 0, 1, 0, 0, 0, 1 };
            var number = NumberSystemOperation.GetDecimalFromBinaryRepresentation(bytesForDecimal);
            Console.WriteLine($"10010001 = {number}\n");

            Console.WriteLine("XOR test");
            var firstBits = new List<byte>
            {
                0, 0, 1, 0, 1, 1, 1, 0
            };
            var secondBits = new List<byte>
            {
                1, 0, 0, 0, 0, 1, 0, 0
            };
            var xorResults = NumberSystemOperation.XOR(firstBits, secondBits);
            OutputResult.PrintBitsRow(xorResults);

            Console.WriteLine("Initial permutation test");
            var tableAfterInitialPermutation = Tables.Permutation(bytesForPermutation.ToList(), Tables.MainInitialPermutation);
            OutputResult.PrintBitsTable(tableAfterInitialPermutation, 16);

            Console.WriteLine("Final permutation test");
            var tableAfterFinalPermutation = Tables.Permutation(tableAfterInitialPermutation, Tables.MainFinalPermutation);
            OutputResult.PrintBitsTable(tableAfterFinalPermutation, 16);

            Console.WriteLine("Generate keys test");
            var keys = new Keys("somekey");
            OutputResult.PrintKeys(keys.EncryptKeys);
        }
    }
}
